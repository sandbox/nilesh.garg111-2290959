<?php

/**
 * @file
 * Admin page callbacks for the hide_drupal_form_message module.
 */

/**
 * Form to config Hide Error Message settings.
 */
function hide_drupal_form_message_admin() {
  $form = array();

  $form['hide_error_msg'] = array(
    '#type' => 'textarea',
    '#title' => t('Error Message To Hide'),
    '#default_value' => _hide_drupal_form_message_admin_convert_array_to_text(variable_get('hidden_error_msg', array())),
    '#description' => t('Hide Error Message configurations, add message or sub string of message one per line.'),
  );
  $form['hide_warning_msg'] = array(
    '#type' => 'textarea',
    '#title' => t('Warning Message To Hide'),
    '#default_value' => _hide_drupal_form_message_admin_convert_array_to_text(variable_get('hidden_warning_msg', array())),
    '#description' => t('Warning Error Message configurations, add message or sub string of message one per line.'),
  );
  $form['hide_status_msg'] = array(
    '#type' => 'textarea',
    '#title' => t('Status Message To Hide'),
    '#default_value' => _hide_drupal_form_message_admin_convert_array_to_text(variable_get('hidden_status_msg', array())),
    '#description' => t('Status Error Message configurations, add message or sub string of message one per line.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}


/**
 * Submit handler for hide_drupal_form_message admin form.
 *
 * Saves hidden_error_msg variable.
 */
function hide_drupal_form_message_admin_submit($form, &$form_state) {
  //Setting the variable after changing it to array
  variable_set('hidden_error_msg', _hide_drupal_form_message_admin_convert_text_to_array($form_state['values']['hide_error_msg']));
  variable_set('hidden_warning_msg', _hide_drupal_form_message_admin_convert_text_to_array($form_state['values']['hide_warning_msg']));
  variable_set('hidden_status_msg', _hide_drupal_form_message_admin_convert_text_to_array($form_state['values']['hide_status_msg']));
  
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Converts a string representation of hide error msg settings to an array.
 *
 * @param $hide_drupal_form_message
 *   A string representation of hide error msg settings.
 *
 * @return
 *   An array representation of hide error msg settings.
 */
function _hide_drupal_form_message_admin_convert_text_to_array($hide_drupal_form_message) {
  $hide_drupal_form_message = preg_split("/(\r\n|\r|\n)/", $hide_drupal_form_message, NULL, PREG_SPLIT_NO_EMPTY);
  return $hide_drupal_form_message;
}

/**
 * Converts an array representation of hide error msg settings to a string.
 *
 * @param $hide_drupal_form_message
 *   An array representation of hide error message settings.
 *
 * @return
 *   A string representation of hide error msg settings.
 */
function _hide_drupal_form_message_admin_convert_array_to_text($hide_drupal_form_message) {
  return implode("\n", $hide_drupal_form_message);
}
