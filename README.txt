INTRODUCTION
------------

 - This Module aims to hide some Error,Warnings and status
   messages.
 - In the admin form provide the string or substring in the particular
   text area to hide the messages containing that string. 


CONFIGURATION
-------------
 
 - Add the string or substring (one entry each line) of the message in the
   particular type of message text area to be hide.